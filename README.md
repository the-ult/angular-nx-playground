# Nx and Angular Sample application

## What

## Why

## Tech / Tools used

- [Nx](https://nx.dev)
- [Angular](https://angular.io)
- [Cypress](https://cypress.io)
- [Testing Library Angular](https://testing-library.com/docs/angular-testing-library/intro/)
- [MSWjs](https://mswjs.io)
- [Zod](https://zod.dev/?id=introduction)

## Resources / Thanks / Shout-outs

- [The Movie DB](https://www.themoviedb.org)
- [The ultimate guide to style content projection in Angular](https://kevinkreuzer.medium.com/the-ultimate-guide-to-style-projected-content-in-angular-731c0721902f)
- [Netanel Basal](https://netbasal.medium.com/)
- [Tim Deschryver](https://timdeschryver.dev/blog/getting-the-most-value-out-of-your-angular-component-tests)
  - Great blogposts about Angular, Testing, and plenty more.
- [Kent C. Dodds](https://kentcdodds.com/blog?q=testing)
  - Creator of Testing Library
  - Testing Jedi
