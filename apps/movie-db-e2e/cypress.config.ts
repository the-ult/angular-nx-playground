import { nxE2EPreset } from '@nrwl/cypress/plugins/cypress-preset';
import { defineConfig } from 'cypress';
import { dirname } from 'node:path';
import { fileURLToPath } from 'node:url';

export default defineConfig({
  e2e: {
    ...nxE2EPreset(dirname(fileURLToPath(import.meta.url))),
    viewportHeight: 1050,
    viewportWidth: 1650,
    env: {
      mock: false,
    },
  },
});
