import { mswMock, mswResetWorker } from '@ult/shared/test/cypress';
import { MoviesPopularPage2 } from '@ult/shared/test/mocks';

describe('Movies Page', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  afterEach(() => mswResetWorker());

  it('should display welcome message', () => {
    const TEST_DATA = MoviesPopularPage2;
    /// Override the default mock-data
    mswMock('movie/popular', TEST_DATA);

    cy.url().should('contain', `/movies`);
    /// ---------------------------------------------------------------
    cy.log('CHECK HEADER');
    /// ---------------------------------------------------------------
    cy.findByRole('heading', { level: 2, name: 'Popular Movies' })
      .should('be.visible')
      .should('have.text', 'Popular Movies');

    /// ---------------------------------------------------------------
    cy.log('CHECK MOVIES');
    /// ---------------------------------------------------------------

    cy.findAllByTestId('movie-page-media-card')
      .should('have.length', 20)
      .each((movie, index) => {
        /// Get the values from our mock-data
        const { id, poster_path, release_date, title, vote_average } = TEST_DATA.results[index];

        /// ---------------------------------------------------------------
        cy.log('  LINK & POSTER');
        /// ---------------------------------------------------------------
        cy.wrap(movie)
          .findByRole('link')
          .should('have.attr', 'href', `/movies/${id}`)
          .findByRole('img')
          .should(
            'have.attr',
            'src',
            `https://image.tmdb.org/t/p/w220_and_h330_face/${poster_path}`
          );

        /// ---------------------------------------------------------------
        cy.log(' VOTE');
        /// ---------------------------------------------------------------
        cy.wrap(movie).findByTestId('movie-score').should('have.text', vote_average);

        /// ---------------------------------------------------------------
        cy.log(' TITLE');
        /// ---------------------------------------------------------------
        cy.wrap(movie)
          .findByRole('heading', {
            level: 4,
            name: title,
          })
          .should('have.text', title);

        /// ---------------------------------------------------------------
        cy.log('  RELEASE DATE');
        /// ---------------------------------------------------------------
        const formattedDate = Intl.DateTimeFormat('en-US', {
          year: 'numeric',
          month: 'short',
          day: 'numeric',
        }).format(new Date(release_date));
        cy.wrap(movie).findByText(formattedDate).should('have.text', formattedDate);
      });

    /// ---------------------------------------------------------------
    cy.log('CHECK MOVIE LINK');
    /// ---------------------------------------------------------------
    cy.findAllByTestId('movie-page-media-card').first().click();
    cy.url().should('contain', `/movies/${TEST_DATA.results[0].id}`);

    cy.findByText(TEST_DATA.results[0].title).should('be.visible');
  });
});
