import { Route } from '@angular/router';

export const MOVIE_DB_ROUTES: Route[] = [
  /// Iets als onderstaande alleen als we meerdere routes hebben in @ult/movie
  // { path: 'movies', loadChildren: () => import('@ult/feature-movies/routes').then((mod) => mod.ADMIN_ROUTES) },
  {
    path: 'movies',
    loadComponent: () => import('@ult/movie/feature-movies').then(({ MoviesPage }) => MoviesPage),
  },
  {
    path: 'movies/:movieId',
    loadComponent: () =>
      import('@ult/movie/feature-movie-detail').then(({ MovieDetailPage }) => MovieDetailPage),
  },
  // {
  //   path: 'details/:movieId',
  // TODO: use async await
  //   loadComponent: () => import('@ult/feature-movie-detail').then(({ MovieDetailPage }) => MovieDetailPage),
  // },
  { path: '', redirectTo: 'movies', pathMatch: 'full' },
  { path: '**', redirectTo: '' },
];
