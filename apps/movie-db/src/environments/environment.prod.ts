import { EnvironmentBase } from '@ult/shared/data-access';

export const environment: EnvironmentBase = {
  production: false,
  baseUrl: 'https://api.themoviedb.org/3',
  environment: 'prd',
  mock: false,
};
