export type MovieType = 'popular' | 'latest' | 'now_playing' | 'top_rated' | 'upcoming';
