import { graphql, rest, SetupWorkerApi } from 'msw';

declare global {
  interface Window {
    Cypress?: unknown;
    msw: {
      graphql: typeof graphql;
      mock: any;
      rest: typeof rest;
      worker: SetupWorkerApi;
    };
  }
  // interface AUTWindow {
  //   msw: {
  //     graphql: typeof graphql;
  //     mock: any;
  //     rest: typeof rest;
  //     worker: SetupWorkerApi;
  //   };
  // }
}
