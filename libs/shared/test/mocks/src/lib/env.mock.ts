import { EnvironmentBase } from '@ult/shared/data-access';

export const ENV_MOCK: EnvironmentBase = {
  production: false,
  baseUrl: 'https://localhost:4200',
  mock: true,
  environment: 'tst',
  appName: 'TEST',
};
