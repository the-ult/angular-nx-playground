import { RequestHandler, setupWorker } from 'msw';
import { HANDLERS } from '../handlers';

export type UNHANDLED_REQUEST_HANDLER = 'bypass' | 'error' | 'warn';

export type Options = {
  mswFile: string;
  onUnhandledRequest: UNHANDLED_REQUEST_HANDLER;
  scope?: string;
};

//  TODO ??: => use the promise, so we can wait for it
export const setupServiceWorkerForBrowser = async (
  // eslint-disable-next-line unicorn/no-object-as-default-parameter
  { mswFile, onUnhandledRequest, scope }: Options = {
    mswFile: 'mockServiceWorker.js',
    onUnhandledRequest: 'bypass',
    scope: '/',
  },
  handlers: RequestHandler[] = HANDLERS
) => {
  const worker = setupWorker(...handlers);

  await worker.start({
    serviceWorker: {
      url: `${mswFile}`,
      options: {
        // Narrow the scope of the Service Worker to intercept requests
        // only from pages under this path.
        // scope: '/__cypress/src/',
        scope,
      },
    },
    // Return the first registered service worker found with the name
    // of `mockServiceWorker`, disregarding all other parts of the URL
    findWorker: (scriptURL) => scriptURL.includes(mswFile),
    onUnhandledRequest,
  });

  return { worker };

  // throw new Error('setupServiceWorkerForBrowser can only be used in a browser context');
};
