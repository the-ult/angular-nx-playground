// import { graphql, rest } from 'msw';
// import { setupServiceWorkerForBrowser } from './browser';

// /**
//  * @see https://mswjs.io/docs/api/setup-worker
//  *
//  * * N.B.
//  * We need a separate file for browser and server. So we cannot
//  * use one index.ts file with both browser and server.
//  */
// export const { worker } = setupServiceWorkerForBrowser();
// export const mock = worker.use;
// export { graphql, rest } from 'msw';

// /**
//  * Add the MSW objects to the window, so we can easily use them with Cypress
//  */
// // eslint-disable-next-line @typescript-eslint/ban-ts-comment
// // @ts-ignore
// window.msw = { graphql, mock, rest, worker };

export { setupServiceWorkerForBrowser } from './browser';
