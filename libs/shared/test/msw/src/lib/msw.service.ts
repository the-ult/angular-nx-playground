import { graphql, rest } from 'msw';

import { inject, Injectable } from '@angular/core';
import { ENVIRONMENT } from '@ult/shared/data-access';
import { setupServiceWorkerForBrowser } from './browser';

/**
 * When we are using the mock environment,
 * we need to check if mswjs.io is available and wait for it to be ready.
 *
 * @see: https://mswjs.io/docs/api/setup-worker/use#examples
 *
 * @usage
 * ```ts
 * {
 *    provide: ENVIRONMENT_INITIALIZER,
 *    multi: true,
 *    useValue() {
 *      inject(MswService).initMswForBrowser();
 *    },
 * },
 * ```
 *
 */
@Injectable({ providedIn: 'root' })
export class MswService {
  async initMswForBrowser() {
    // !FIXME: use other environment variable
    // !FIXME: => seems to be needed voor E2E only?
    const { mock, production } = inject(ENVIRONMENT);

    if (mock && !production) {
      const { worker } = await setupServiceWorkerForBrowser();
      const mock = worker.use;

      /**
       * Add the MSW objects to the window, so we can easily use them with Cypress
       */
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      window.msw = { graphql, mock, rest, worker };
    }
  }
}
